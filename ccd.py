#!/usr/bin/python
"""Reduction of Flarecam images
http://www.apo.nmsu.edu/Telescopes/ARCSAT/Instruments/arcsat_instruments.html
"""

import ccdproc
from astropy import units as u
from glob import glob
from os.path import join
import numpy as np
from astropy.io import fits

datadir = "/home/miguel/project/ARCSAT/data"
filter = "R"

images = glob(join(datadir, 'domeflat_{0}_???.fits'.format(filter)))
ccds = []
for fi in images:
    ccds.append(fits.open(fi)[0].data)
master_flat = ccdproc.CCDData(np.median(ccds, axis=0), unit=u.adu)
master_flat = ccdproc.gain_correct(master_flat, 1.25*u.electron/u.adu)

images = glob(join(datadir, 'Dark_BIN1_*.fits'.format(filter)))
ccds = []
for fi in images:
    ccds.append(fits.open(fi)[0].data)
master_dark = ccdproc.CCDData(np.median(ccds, axis=0), unit=u.adu)
master_dark = ccdproc.gain_correct(master_dark, 1.25*u.electron/u.adu)
master_dark.header['exposure'] = 300.0

images = glob(join(datadir, '0041P_{0}_20170607_*.fits'.format(filter)))

for fi in images:
    print(fi)
    hdus = fits.open(fi)
    ccd = ccdproc.CCDData(hdus[0].data, unit=u.adu)
    ccd.header['exposure'] = 300.0
    nccd = ccdproc.ccd_process(ccd,
                            error=True,
                            gain=1.25*u.electron/u.adu,
                            readnoise=11.8*u.electron,
                            dark_frame=master_dark,
                            exposure_key='exposure',
                            exposure_unit=u.second,
                            dark_scale=True,
                            master_flat=master_flat)
    fits.writeto(fi.replace('.fits', '_red.fits'), nccd, hdus[0].header,
                 overwrite=True)
